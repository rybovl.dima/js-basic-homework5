// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function isValidNumber(value) {
    return !isNaN(value);
}

function getNumberFromUser(message) {
    let input;
    do {
        input = prompt(message);
    } while (!isValidNumber(input));
    return parseFloat(input);
}

let num1 = getNumberFromUser("Введіть перше число:");
let num2 = getNumberFromUser("Введіть друге число:");

let operator;
do {
    operator = prompt("Введіть математичну операцію (+, -, *, /):");
} while (!['+', '-', '*', '/'].includes(operator));

function calculate(num1, num2, operator) {
    switch (operator) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            if (num2 !== 0) {
                return num1 / num2;
            } else {
                return "Неможливо ділити на нуль!";
            }
        default:
            return "Такої операції не існує";
    }
}

console.log("Результат:", calculate(num1, num2, operator));

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.      

function isThisValidNumber(value) {
    return !isNaN(value);
}

function getNumberFromYou(message) {
    let input;
    do {
        input = prompt(message);
    } while (!isThisValidNumber(input));
    return parseFloat(input);
}

let firstNumber = getNumberFromYou("Введіть перше число:");
let secondNumber = getNumberFromYou("Введіть друге число:");

let operation;
do {
    operation = prompt("Введіть математичну операцію (+, -, *, /):");
    if (!['+', '-', '*', '/'].includes(operation)) {
        alert('Такої операції не існує');
    }
} while (!['+', '-', '*', '/'].includes(operation));

function calculate(firstNumber, secondNumber, operation) {
    switch (operation) {
        case '+':
            return firstNumber + secondNumber;
        case '-':
            return firstNumber - secondNumber;
        case '*':
            return firstNumber * secondNumber;
        case '/':
            if (secondNumber !== 0) {
                return firstNumber / secondNumber;
            } else {
                return "Неможливо ділити на нуль!";
            }
    }
}

console.log("Результат:", calculate(firstNumber, secondNumber, operation));


// Реалізувати функцію підрахунку факторіалу числа.

const getNumberFromPeople = () => {
    let input;
    do {
        input = prompt("Введіть число:");
    } while (!Number.isInteger(+input) || +input < 0); 
    return +input;
};

const factorial = (n) => {
    if (n === 0 || n === 1) {
        return 1;
    }
    return n * factorial(n - 1);
};

const number = getNumberFromPeople();

console.log(`Факторіал числа ${number} дорівнює:`, factorial(number));